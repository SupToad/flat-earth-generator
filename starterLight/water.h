#ifndef WATER_H
#define WATER_H


#include <biome.h>
#include <vector>
#include <iostream>

/**
 * @brief Child class of Biome representing water
 */
class Water : public Biome
{
private:
    static constexpr float min = -0.5;
    static constexpr float max = 0.5;
    static constexpr float variation = 0.7;
    static constexpr float base = -1.3;
public:
    Water();
    float getMin();
    float getMax();
    float getVariation();
    float getBase();
    void show();
};

#endif // WATER_H
