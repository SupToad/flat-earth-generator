#ifndef PLAINS_H
#define PLAINS_H


#include <biome.h>
#include <vector>
#include <random>
#include <iostream>

/**
 * @brief Child class of Biome representing a plain
 */
class Plains : public Biome
{
private:
    static constexpr float min = 0.45;
    static constexpr float max = 0.9;
    static constexpr float variation = 0.6;
    static constexpr float base = 0.6;

public:
    Plains();
    float getMin();
    float getMax();
    float getVariation();
    float getBase();
    void show();
};

#endif // PLAINS_H
