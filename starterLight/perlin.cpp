#include "perlin.h"

Perlin::Perlin()
    : base_(nullptr), use_base_(false), xmax_(0), ymax_(0)
{
}

Perlin::~Perlin()
{
    if (use_base_)
        delete[] base_;
    base_ = nullptr;
    use_base_ = false;
}

void Perlin::fillBase(float *base, unsigned int xmax, unsigned int ymax)
{
    use_base_ = true;
    xmax_ = xmax;
    ymax_ = ymax;
    base_ = new float[xmax_*ymax_];

    std::copy(base, base + (xmax * ymax), base_);
}

float Perlin::base_noise(int a, int b)
{
    return base_[a + b*xmax_];
}

float Perlin::rand_noise_x(int t)
{
    float r = sin(float(t)/100) * rand_x;
    return r - int(r);
}

float Perlin::rand_noise_y(int t)
{
    float r = sin(float(t)/100) * rand_y;
    return r - int(r);
}

float Perlin::noise_2d(int x, int y)
{
    float r;
    if (use_base_ && x < xmax_ && y < ymax_)
        r = base_noise(x,y);
    else
    {
        int tmp = int(rand_noise_x(x) * rand_noise_y(y) * 850000);
        r = rand_noise_x(tmp);
    }

    return r;
}

float Perlin::cosineInterpolation(float a, float b, float t)
{
    float c = (1.0f - cosf(t * M_PI)) * .5f;

    return (1.0f - c) * a + c * b;
}

float Perlin::smooth_noise(float x, float y)
{
  //Partie entière : E(x)
  int integer_x = int(x);
  int integer_y = int(y);

  //Partie fractionnaire : x - E(x)
  float fractional_x = x - integer_x;
  float fractional_y = y - integer_y;

  //Bruit des quatre points d'un cube
  float a = noise_2d(integer_x,     integer_y);
  float b = noise_2d(integer_x+1,     integer_y);
  float c = noise_2d(integer_x,     integer_y+1);
  float d = noise_2d(integer_x+1,     integer_y + 1);

  float f = interpolation(a, b, fractional_x);
  float g = interpolation(c, d, fractional_x);

  return interpolation(f, g, fractional_y);
}

float Perlin::interpolation(float a, float b, float t)
{
    return cubicHermineCurveInterpolation(a,b,t);
}

float Perlin::linearInterpolation(float a, float b, float t)
{
    return (1.0f - t) * a + t * b;
}

float Perlin::cubicHermineCurveInterpolation(float a, float b, float t)
{
    float l = -2*t*t*t+3*t*t;
    return (1-l) * a + l * b;
}

float Perlin::perlin(int octaves, float frequency, float persistence, float x, float y)
{

//    srand(time(NULL));
//    rand_x = (rand()%9999999)/1000;
//    rand_y = (rand()%9999999)/1000;

    float r = 0.;
    float f = frequency;
    float amplitude = 1.;

    for(int i = 0; i < octaves; i++)
    {
        int t = i * 4096;
        r += smooth_noise(x * f+t, y * f+t) * amplitude;
        amplitude *= persistence;
        f *= 2;
    }

    float geo_lim = (1 - persistence) / (1 - amplitude);

    return r * geo_lim;
}

void Perlin::setRand(QString s)
{
    int v = 0;
    for (unsigned i = 0 ; i < s.length() ; ++i)
        v += s.at(i).toLatin1();
    rand_x = float(v)/1000.0f;
    rand_y = float(v)/118.0f-1000.0f;
}

