#include "diamondsquare.h"

DiamondSquare::DiamondSquare()
{

}

std::vector<std::vector<double>> DiamondSquare::squareDiamond (int nbSommetsCote){
    if((nbSommetsCote - 1) % 2 != 0) exit(-1);

    int h = nbSommetsCote;
    std::vector<std::vector<double>> t(h, std::vector<double>(h, 0.0));

    // Création de la distribution aléatoire
    std::default_random_engine rng;
    std::uniform_real_distribution<double> dist((float)-h, (float)h);

    // Initialisation des coins
    t[0][0] = dist(rng);
    t[0][h - 1] = dist(rng);
    t[h - 1][h - 1] = dist(rng);
    t[h - 1][0] = dist(rng);

    // Initialisation de i à lindex du dernier élément d'une ligne/colonne
    int i = h - 1;

    //Tant que i > 1 (soit tant que tous les éléments du tableau n'ont pas été parcourus)
    while ( i > 1 ) {
        //On prend le milieu de 0...i et on l'utilise pour créer des valeurs aléatoires
        int index = i / 2;
        std::uniform_real_distribution<double> dist2((float)-index, (float)index);

        /*Phase diamant: On attribue au centre de chaque carré la moyenne des quatre sommets de celui-ci
        + une valeur aléatoire basée sur l'index courant*/
        for (int x = index ; x <= h - 1 ; x += i){
            for (int y = index ; y <= h - 1; y += i){
                double mean = (t[x - index][y - index] +
                        t[x - index][y + index] +
                        t[x + index][y + index] +
                        t[x + index][y - index]) / 4;

                t[x][y] = mean + dist2(rng);
            }
        }


        /*Phase carré =  En partant du centre et des sommets de chaque carré, on initialise les sommets du
        losange correspondant (celui dont les sommets correspondent au mileu de chaque côté du carré)*/
        int decal = 0;
        for (int x = 0 ; x <= h-1 ; x += index){
            if(decal = 0) decal = index;
            else decal = 0;

            for (int y = decal ; y <= h-1 ; y += i){
                double sum = 0;
                int n = 0;
                if(x >= index){
                    sum += t[x - index][y];
                    ++n;
                }
                if(x + index < h){
                    sum += t[x + index][y];
                    ++n;
                }
                if(y >= index){
                    sum += t[x][y - index];
                    ++n;
                }
                if(y + index < h){
                    sum += t[x][y + index];
                    ++n;
                }
                t[x][y] = sum / n + dist2(rng);
            }
        }
        //On attribue index à i, ce qui revient à diviser le pas par 2 à chaque itération
        i = index;
    }
    return t;
}

std::vector<std::vector<Biome*>> DiamondSquare::biomeRepartition(int nbSommetsCote)
{
    std::vector<std::vector<double>> ds_result = this->squareDiamond(nbSommetsCote);
    double sum = 0.0;
    std::vector<std::vector<Biome*>> biomes(nbSommetsCote, std::vector<Biome*>(nbSommetsCote));

    for(int i = 0 ; i < ds_result.size() ; ++i) {
        for(int j = 0 ; j < ds_result[i].size() ; ++j) {
            sum += fabs(ds_result[i][j]);
        }
    }

    //De la valeur la plus faible à la valeur au "quart" des valeurs = Eau
    //De la valeur "trois-quart" au quart des valeurs jusqu'à la valeur la plus élevée = Montagne
    //Au milieu = Plaine
    double breakpointOne = sum / (nbSommetsCote * nbSommetsCote) / 2;
    double breakpointTwo = sum / (nbSommetsCote * nbSommetsCote) + (sum / (nbSommetsCote * nbSommetsCote) / 2);

    for(int i = 0 ; i < ds_result.size() ; ++i) {
        for(int j = 0 ; j < ds_result[i].size() ; ++j) {
            if(fabs(ds_result[i][j]) < breakpointOne){
                biomes[i][j] = new Water();
            }

            else if(fabs(ds_result[i][j]) > breakpointTwo){
                biomes[i][j] = new Mountain();
            }

            else{
                biomes[i][j] = new Plains();
            }
        }
    }

    return biomes;
}

std::vector<float> DiamondSquare::baseForPerlin(int nbSommetsCote, std::vector<std::vector<Biome*>> biomes)
{
    //std::vector<std::vector<Biome*>> biomes = biomeRepartition(nbSommetsCote);
    std::vector<float> base(nbSommetsCote * nbSommetsCote);
    for(int j = 0 ; j < nbSommetsCote ; ++j){
        for(int i = 0 ; i < nbSommetsCote ; ++i){
            base[i + j * nbSommetsCote] = biomes[i][j]->getBase();
        }
    }

    return base;
}

std::vector<Biome*> DiamondSquare::biomesAsSimpleVector(int nbSommetsCote, std::vector<std::vector<Biome*>> biomes){
    std::vector<Biome*> biomesVector(nbSommetsCote * nbSommetsCote);
    for(int j = 0 ; j < nbSommetsCote ; ++j){
        for(int i = 0 ; i < nbSommetsCote ; ++i){
            biomesVector[i + j * nbSommetsCote] = biomes[i][j];
        }
    }

    return biomesVector;
}
