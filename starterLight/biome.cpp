#include "biome.h"

Biome::Biome()
{

}

float Biome::transform(float v)
{
    return getMin() + v * getVariation() * (getMax() - getMin());
}
