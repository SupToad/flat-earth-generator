#ifndef MOUNTAIN_H
#define MOUNTAIN_H


#include <biome.h>
#include <vector>
#include <random>
#include <iostream>


/**
 * @brief Child class of Biome representing a mountain
 */
class Mountain : public Biome
{
private:
    static constexpr float min = 0.9;
    static constexpr float max = 1.5;
    static constexpr float variation = 0.8;
    static constexpr float base = 0.8;
public:
    Mountain();
    float getMin();
    float getMax();
    float getVariation();
    float getBase();
    void show();
};

#endif // MOUNTAIN_H
