#ifndef DIAMONDSQUARE_H
#define DIAMONDSQUARE_H

#include <vector>
#include <random>
#include <chrono>
#include <memory>
#include <cmath>
#include "biome.h"
#include "mountain.h"
#include "water.h"
#include "plains.h"

/**
 * @brief Functions relative to the Diamond-Square algorithm
 */
class DiamondSquare
{
public:
    DiamondSquare();

    /**
     * @brief For each entry in a matrix of nbSommetsCote * nbSommetsCote, attributes a value according to the diamond-square
     * algorithm
     * @param nbSommetsCote, numbers of vertices on one side of the mesh
     * @return A matrix of double, representing the values that the diamond-square algorithm attributed to each vertex of the mesh
     */
    std::vector<std::vector<double>> squareDiamond (int nbSommetsCote);

    /**
     * @brief Executes the squareDiamond(int nbSommetsCote) function, and attributes a biome to each entry of a nbSommetsCote * nbSommetsCote,
     * according to the value of the associated entry in the return of the squareDiamond(int nbSommetsCote) function
     * @param nbSommetsCote, numbers of vertices on one side of the mesh
     * @return A matrix of objects of the children classes of Biome
     */
    std::vector<std::vector<Biome*>> biomeRepartition(int nbSommetsCote);

    /**
     * @brief Vetorizes a matrix of Biomes, such as the base value of each biome in the 2-Dimensional matrix biomes
     * is replaced by the value of its getBase() function (see Biome documentation) in a corresponding 1-Dimensional vector.
     * @param nbSommetsCote, numbers of vertices on one side of the mesh
     * @param biomes, a nbSommetsCote*nbSommetsCote matrix of biomes
     * @return a vector of float containing the base value of each biome from the biomes matrix
     */
    std::vector<float> baseForPerlin(int nbSommetsCote, std::vector<std::vector<Biome*>> biomes);

    /**
     * @brief Vetorizes a matrix of Biomes
     * @param nbSommetsCote, numbers of vertices on one side of the mesh
     * @param biomes, a nbSommetsCote*nbSommetsCote matrix of biomes
     * @return a vector of biomes
     */
    std::vector<Biome*> biomesAsSimpleVector(int nbSommetsCote, std::vector<std::vector<Biome*>> biomes);
};

#endif // DIAMONDSQUARE_H
