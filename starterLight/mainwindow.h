#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileDialog>
#include <QMainWindow>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <random>
#include <vector>
#include <iostream>
#include <memory>
#include <set>
#include<random>
#include<chrono>

#include <QVector3D>

#include "perlin.h"
#include "diamondsquare.h"
#include "biome.h"

namespace Ui {
class MainWindow;
}

using namespace OpenMesh;
using namespace OpenMesh::Attributes;

struct MyTraits : public OpenMesh::DefaultTraits
{
    // use vertex normals and vertex colors
    VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
    // store the previous halfedge
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
    // use face normals face colors
    FaceAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
    EdgeAttributes( OpenMesh::Attributes::Color | OpenMesh::Attributes::Status );
    // vertex thickness
    VertexTraits{float thickness; float value;};
    // edge thickness
    EdgeTraits{float thickness;};
};
typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;

/**
 * @brief Main class of the application
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /**
     * @brief Window creation
     * @param parent is set to 0, as this QObject is the root of the interface
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * @brief Display the mesh with the attributes associated to each OpenMesh element. Written by Arnaud Polette.
     * @param _mesh
     * @param isTemperatureMap
     * @param mapRange
     */
    void displayMesh(MyMesh *_mesh, bool isTemperatureMap = false, float mapRange = -1);

    /**
     * @brief Removes all colors and non-base thickness from the mesh. Written by Arnaud Polette.
     * @param _mesh
     */
    void resetAllColorsAndThickness(MyMesh* _mesh);

    /**
     *
     * @brief Generates a heightmap, based either on Perlin noise alone or on a Perlin noise applied to a base biomes
     * distributed by the Diamond-Square algorithm
     * @param _mesh
     * @param nbFacesCote, number of quadragular faces on each side of the generated terrain
     */
    void heightmap (MyMesh* _mesh, int nbFacesCote);

    /**
     * @brief Iterates on each face of the mesh and color it by calling the colorFace(MyMesh *_mesh, FaceHandle fh) function
     * @param _mesh
     */
    void color (MyMesh *_mesh);

    /**
     * @brief Colors a face according to it's height
     * @param _mesh
     * @param fh, Face to color
     */
    void colorFace (MyMesh *_mesh, FaceHandle fh);

    /**
     * @brief Transform the vertex currently selected to the chosen biome
     * @param _mesh
     * @param vh, Vertex to transform
     * @param b, target biome
     */
    void transformVertex(MyMesh* _mesh, VertexHandle vh, Biome* b);


private slots:

    /**
     * @brief Generates flat terrain based only on Perlin noise
     */
    void on_pushButton_generer_clicked();

    /**
     * @brief Generates terrain on a sphere, based on Perlin noise
     */
    void on_pushButton_sphere_clicked();

    /**
     * @brief Change the number of quadrangular faces on each side of the terrain to generate
     * @param arg1, new value
     */
    void on_spinBox_valueChanged(int arg1);

    /**
     * @brief
     * @param arg1, new value
     */
    void on_spinBox_oct_valueChanged(int arg1);

    /**
     * @brief
     * @param arg1, new value
     */
    void on_doubleSpinBox_per_valueChanged(double arg1);

    /**
     * @brief
     * @param arg1, new value
     */
    void on_doubleSpinBox_fre_valueChanged(double arg1);

    /**
     * @brief Generates terrain. First, creates a biome repartition using DiamondSquare, and the associated
     * vector of base values. Then, applies a Perlin noise on those base values
     */
    void on_pushButton_ds_clicked();

    /**
     * @brief Selects a vertex from the mesh
     * @param arg1, new value
     */
    void on_vertexSelect_spinBox_valueChanged(int arg1);

    /**
     * @brief Transform the selected vertex into a Mountain
     */
    void on_toMountain_pushButton_clicked();

    /**
     * @brief Transform the selected vertex into a Plain
     */
    void on_toPlains_pushButton_clicked();

    /**
     * @brief Transform the selected vertex into Water
     */
    void on_toWater_pushButton_clicked();

private:
    /**
     * @brief Vertex currently selected
     */
    int vertexSelection = -1;

    /**
     * @brief Number of quadrangular faces on each side of the mesh to generate
     */
    int nbFacesCote = 128;

    /**
     * @brief octValue
     */
    int octValue = 10;

    /**
     * @brief perValue
     */
    double perValue = 0.5;

    /**
     * @brief freValue
     */
    double freValue = 0.25;

    /**
     * @brief Boolean use to determine if the terrain generation should be based on a biome repartition
     * based on the Diamond-Square algoritmh or not
     */
    bool useDs = false;

    /**
     * @brief Boolean used to determine if the terrain generated shpuld be a plane or a sphere
     */
    bool isSphere = false;

    /**
     * @brief The generated mesh
     */
    MyMesh mesh;

    /**
     * @brief List of biomes distributed by Diamond-Square
     */
    std::vector<std::vector<Biome*>> biomes;

    /**
     * @brief List of biomes distributed by Diamond-Square, arranged in a 1-Dimensional vector
     */
    std::vector<Biome*> biomesVector;

    /**
     * @brief DiamondSquare object
     */
    DiamondSquare ds;

    /**
     * @brief Random engine used to randomize the presence of forests
     */
    std::default_random_engine r = std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());

    /**
     * @brief Distribution used to randomize the presence of forests
     */
    std::uniform_int_distribution<int> d{1, 10};

    Ui::MainWindow *ui;
    OpenMesh::VPropHandleT<float>  vprop_coeff;
};

#endif // MAINWINDOW_H
