#ifndef BIOME_H
#define BIOME_H

#include <vector>
#include <random>
#include <QDebug>

/**
 * @brief The biome class is a virtual class representing a generalization for types of terrain
 */
class Biome
{
private:
public:
    Biome();
    /**
     * @brief returns min height value for a given type of biome. Used to transform a point in the specified biome.
     * @return min height value for a given type of biome
     */
    virtual float getMin() = 0;

    /**
     * @brief returns max height value for a given type of biome. Used to transform a point in the specified biome.
     * @return max height value for a given type of biome
     */
    virtual float getMax() = 0;

    /**
     * @brief returns variation in height value for a given type of biome. Used to transform a point in the specified biome.
     * @return variation in height value for a given type of biome
     */
    virtual float getVariation() = 0;

    /**
     * @brief returns the base height value of a given type of biome. Used to give a base value to points before the application of Perlin Noise
     * @return base height value for a given type of biome
     */
    virtual float getBase() = 0;

    /**
     * @brief Prints the type of the biome
     */
    virtual void show() = 0;

    /**
     * @brief Adjust some height to match the Biome height.
     * @param v, height value generated
     * @return v after being adjusted
     */
    float transform(float v);
};

#endif // BIOME_H
