#ifndef PERLIN_H
#define PERLIN_H

#include <math.h>
#include <qdebug.h>
#include <QVector2D>
#include <iostream>

/** @brief Functions relative to the Perlin noise. */
class Perlin
{
private:
    /** @brief Array for pre-generated terrain to modify */
    float* base_;
    /** @brief Determine if we use a base or generate one.
        @value true if we initiated base_ (see fillBase(float* base, unsigned xmax, unsigned ymax)), false otherwise.
    */
    bool use_base_;
    /** @brief base_'s x value */
    unsigned xmax_;
    /** @brief base_'s y value */
    unsigned ymax_;

    /** @brief Seed for X values */
    float rand_x;
    /** @brief Seed for Y values */
    float rand_y;
public:
    Perlin();
    virtual ~Perlin();

    /**
     * @brief Fill base_.
     * @param base array to copy
     * @param xmax x value for base
     * @param ymax y value for base
     */
    void fillBase(float* base, unsigned xmax, unsigned ymax);
    /**
     * @brief Reset base_.
     */
    void cleanBase() {delete[] base_; use_base_ = false;}
    /**
     * @brief Generate a noise value based on base_ array.
     * @param a x
     * @param b y
     * @return Noise value at (a,b).
     */
    float base_noise(int a, int b);

    /**
     * @brief Generate perlin noise.
     * @param octaves Number of noise to add to form the turbulence
     * @param frequency Determine the number of cycle. Doubled at each octave.
     * @param persistence Determine how quickly the amplitudes diminish
     * @param x X value
     * @param y Y value
     * @return Perlin noise value at (x,y).
     */
    float perlin(int octaves, float frequency,
                 float persistence, float x, float y);

    float interpolation(float a, float b, float t);

    /**
     * @brief Interpolate using only t value (t)
     * @param a
     * @param b
     * @param t
     * @return Interpolation between a and b for t.
     */
    float linearInterpolation(float a, float b, float t);

    /**
     * @brief Interpolate using the cosinus (cos(t * M_PI))
     * @param a
     * @param b
     * @param t
     * @return Interpolation between a and b for t.
     */
    float cosineInterpolation(float a, float b, float t);

    /**
     * @brief Interpolate using the Hermine Curve (-2*t³+3*t²)
     * @param a
     * @param b
     * @param t
     * @return Interpolation between a and b for t.
     */
    float cubicHermineCurveInterpolation(float a, float b, float t);

    /**
     * @brief Generate noise value based on rand_x.
     * @param t X value
     * @return Noise.
     */
    float rand_noise_x(int t);

    /**
     * @brief Generate noise value based on rand_y.
     * @param t Y value
     * @return Noise.
     */
    float rand_noise_y(int t);

    /**
     * @brief Chooses between the rand_noise or base_noise
     * @param x
     * @param y
     * @return Generated noise.
     */
    float noise_2d(int x, int y);

    /**
     * @brief Generate the 2D noise from noise interpolations
     * @param x
     * @param y
     * @return Noise value at (x,y)
     */
    float smooth_noise(float x, float y);

    /**
     * @brief Set the rand_x & rand_y value from the seed.
     * @param s Seed from the UI.
     */
    void setRand(QString s);
};

#endif // PERLIN_H
