#include "mainwindow.h"
#include "ui_mainwindow.h"

/* **** début de la partie boutons et IHM **** */

void MainWindow::heightmap (MyMesh* _mesh, int nbFacesCote)
{
    Perlin perlin;
    perlin.setRand(ui->line_Seed->text());

    int nbSommetsCotes = nbFacesCote + !isSphere;

    if(useDs){
        biomes = ds.biomeRepartition(nbSommetsCotes);
        biomesVector = ds.biomesAsSimpleVector(nbSommetsCotes, biomes);
        std::vector<float> baseAsVector = ds.baseForPerlin(nbSommetsCotes, biomes);
        float base[nbSommetsCotes * nbSommetsCotes];
        std::copy(baseAsVector.begin(), baseAsVector.end(), base);
        perlin.fillBase(base, nbSommetsCotes, nbSommetsCotes);
    }

    qDebug() << nbSommetsCotes << nbSommetsCotes*nbSommetsCotes << _mesh->n_vertices();
    _mesh->add_property(vprop_coeff, "vprop_coeff");
//    if (!isSphere)
    {
        for( int i = 0 ; i < nbSommetsCotes ; ++ i) {
            for ( int j = 0 ; j < nbSommetsCotes ; ++j ) {
                MyMesh::Point h;
                MyMesh::VertexHandle vh = _mesh->vertex_handle(i * (nbSommetsCotes) + j);
                float v = perlin.perlin(octValue, freValue, perValue, i, j) + !isSphere;
                h = v * _mesh->normal(vh);
                _mesh->property(vprop_coeff, vh) = v;
//                qDebug() << v
//                         << _mesh->normal(vh)[0] << _mesh->normal(vh)[1] << _mesh->normal(vh)[2]
//                         << h[0] << h[1] << h[2];
//                Mountain w;
//                h[1] = w.transform(h[1]);

                _mesh->set_point(vh, _mesh->point(vh) + h);
            }
        }

        if (useDs)
        {
            perlin.cleanBase();
            useDs = false;
        }
    }

}

void MainWindow::colorFace(MyMesh* _mesh, FaceHandle fh)
{
    float yMoyen = 0;
    std::vector<bool> areVerticesForests;
    bool forest = false;
    // parcours des sommets de la face courante
    for (MyMesh::FaceVertexIter curVert = _mesh->fv_iter(fh); curVert.is_valid(); curVert++)
    {
       yMoyen = (yMoyen + (!isSphere*2-1) * _mesh->property(vprop_coeff, *curVert)); //_mesh->point(*curVert)
       int val = d(r);

       if(val <= 5) areVerticesForests.push_back(true);
       else areVerticesForests.push_back(false);
    }

    forest = areVerticesForests[0] ?
                (areVerticesForests[1] || areVerticesForests[2]) :
                (areVerticesForests[1] && areVerticesForests[2]);

    yMoyen = yMoyen / 3.0 + isSphere; // calcul du Y moyen sur la face
//        qDebug() << yMoyen;

    // on change la couleur selon le Y moyen
    if (yMoyen > 1.25)
        _mesh->set_color(fh, MyMesh::Color(225, 225, 225));
    else if (yMoyen > 0.9)
    {
        if(forest) _mesh->set_color(fh, MyMesh::Color(127, 127, 50));
        else _mesh->set_color(fh, MyMesh::Color(127, 50, 50));

    }
    else if (yMoyen > 0.5)
    {
        if(forest) _mesh->set_color(fh, MyMesh::Color(0, 255, 0));
        else _mesh->set_color(fh, MyMesh::Color(127, 255, 127));
    }
    else if (yMoyen > 0.2)
        _mesh->set_color(fh, MyMesh::Color(127, 127, 255));
    else
        _mesh->set_color(fh, MyMesh::Color(20, 20, 200));

}

void MainWindow::color (MyMesh* _mesh)
{
    // parcours des faces
    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        colorFace(_mesh, *curFace);
    }
}

// exemple pour construire un mesh face par face
void MainWindow::on_pushButton_generer_clicked()
{
    useDs = false;
    isSphere = false;
    MyMesh mesh;
    int nbFaces = nbFacesCote * nbFacesCote;
    int nbSommetsCote = nbFacesCote + 1;
    int nbSommets = (nbSommetsCote) * (nbSommetsCote);

    // on construit une liste de sommets
    MyMesh::VertexHandle sommets[nbSommets];

    for(int i = 0 ; i < nbSommetsCote ; ++i){
        for(int j = 0 ; j < nbSommetsCote ; ++j ){
            sommets[i*(nbSommetsCote)+j] = mesh.add_vertex(MyMesh::Point(
                ((float) j*(2.0f/((float)nbSommetsCote - 1.0f)) - 1.0f) * (floor((double)nbFacesCote / 10.0)),
                0,
                ((float) i*(2.0f/((float)nbSommetsCote - 1.0f)) - 1.0f) * (floor((double)nbFacesCote / 10.0))));
        }
    }


    // on construit des faces à partir des sommets

    for (int i = 0 ; i < nbFacesCote ; ++i) {
        for (int j = 0; j < nbFacesCote; ++j) {


            std::vector<MyMesh::VertexHandle> uneNouvelleFace;

            uneNouvelleFace.push_back(sommets[nbSommetsCote * i + j]);
            uneNouvelleFace.push_back(sommets[nbSommetsCote * i + j + 1]);
            uneNouvelleFace.push_back(sommets[nbSommetsCote * (i + 1) + j + 1]);
            uneNouvelleFace.push_back(sommets[nbSommetsCote * (i + 1) + j]);

            mesh.add_face(uneNouvelleFace);
        }
    }

    mesh.update_normals();


    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // on calcule la heightmap
    heightmap(&mesh, nbFacesCote);

    color(&mesh);

    // on affiche le maillage
    displayMesh(&mesh);
    this->mesh = mesh;
    ui->vertexSelect_spinBox->setMaximum(mesh.n_vertices());
}

// exemple pour construire un mesh face par face
void MainWindow::on_pushButton_sphere_clicked()
{
    useDs = false;
    isSphere = true;
    MyMesh mesh;

    unsigned stack_count = nbFacesCote;
    unsigned slice_count = nbFacesCote;
    unsigned nb_tri = (stack_count-1)*(slice_count)*2;
    float rayon = 5.0f;
    MyMesh::VertexHandle sommets[nb_tri];

    for (unsigned j = 0 ; j < stack_count-1 ; ++j)
    {
        for (unsigned i = 0 ; i < slice_count ; ++i)
        {
            float x, y, z;
            float theta  = -M_PI+(2*M_PI*j/(stack_count-1));
            float lambda = -M_PI/2+(M_PI*i/(slice_count-1)) ;
            x = rayon * cos(theta)*cos(lambda);
            y = rayon * sin(theta)*cos(lambda);
            z = rayon * sin(lambda);

            sommets[i+j*stack_count] = mesh.add_vertex(MyMesh::Point(x,y,z));
        }
    }

    for (unsigned j = 0 ; j < stack_count ; ++j)
    {
        std::vector<MyMesh::VertexHandle> face0;
        face0.push_back(sommets[0]);
        face0.push_back(sommets[j*slice_count+1]);
        face0.push_back(sommets[(j+1)%(stack_count-1)*slice_count+1]);
        mesh.add_face(face0);
        std::vector<MyMesh::VertexHandle> face1;
        face1.push_back(sommets[(j+1%(stack_count-1))*slice_count+1]);
        face1.push_back(sommets[(j+1)%(stack_count-1)*slice_count+1]);
        face1.push_back(sommets[0]);
        mesh.add_face(face1);

        for (unsigned i = 1 ; i < slice_count-2 ; ++i)
        {

            std::vector<MyMesh::VertexHandle> uneNouvelleFace;
            uneNouvelleFace.push_back(sommets[j*slice_count+i]);
            uneNouvelleFace.push_back(sommets[j*slice_count+(i+1)]);
            uneNouvelleFace.push_back(sommets[(j+1)%(stack_count-1)*slice_count+(i+1)]);
            mesh.add_face(uneNouvelleFace);

            uneNouvelleFace.clear();
            uneNouvelleFace.push_back(sommets[(j+1)%(stack_count-1)*slice_count+(i+1)]);
            uneNouvelleFace.push_back(sommets[(j+1)%(stack_count-1)*slice_count+i]);
            uneNouvelleFace.push_back(sommets[j*slice_count+i]);
            mesh.add_face(uneNouvelleFace);

        }

        face0.clear();

        face0.push_back(sommets[j*slice_count+slice_count-2]);
        face0.push_back(sommets[slice_count-1]);
        face0.push_back(sommets[(j+1)%(stack_count-1)*slice_count+slice_count-2]);
        mesh.add_face(face0);
    }

    mesh.update_normals();


    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // on calcule la heightmap
    heightmap(&mesh, nbFacesCote-1);

    color(&mesh);

    // on affiche le maillage
    displayMesh(&mesh);
    this->mesh = mesh;

    ui->vertexSelect_spinBox->setMaximum(mesh.n_vertices());

}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    nbFacesCote = arg1;
    qDebug() << nbFacesCote;
}

void MainWindow::on_spinBox_oct_valueChanged(int arg1)
{
    octValue = arg1;
}

void MainWindow::on_doubleSpinBox_fre_valueChanged(double arg1)
{
    freValue = arg1;
    qDebug() << freValue;
}

void MainWindow::on_doubleSpinBox_per_valueChanged(double arg1)
{
    perValue = arg1;
    qDebug() << perValue;
}

void MainWindow::on_pushButton_ds_clicked()
{
    useDs = true;
    isSphere = false;
    MyMesh mesh;
    int nbFaces = nbFacesCote * nbFacesCote;
    int nbSommetsCote = nbFacesCote + 1;
    int nbSommets = (nbSommetsCote) * (nbSommetsCote);

    // on construit une liste de sommets
    MyMesh::VertexHandle sommets[nbSommets];

    for(int i = 0 ; i < nbSommetsCote ; ++i){
        for(int j = 0 ; j < nbSommetsCote ; ++j ){
            sommets[i*(nbSommetsCote)+j] = mesh.add_vertex(MyMesh::Point(
                ((float) j*(2.0f/((float)nbSommetsCote - 1.0f)) - 1.0f) * (floor((double)nbFacesCote / 10.0)),
                0,
                ((float) i*(2.0f/((float)nbSommetsCote - 1.0f)) - 1.0f) * (floor((double)nbFacesCote / 10.0))));
        }
    }


    // on construit des faces à partir des sommets

    for (int i = 0 ; i < nbFacesCote ; ++i) {
        for (int j = 0; j < nbFacesCote; ++j) {


            std::vector<MyMesh::VertexHandle> uneNouvelleFace;

            uneNouvelleFace.push_back(sommets[nbSommetsCote * i + j]);
            uneNouvelleFace.push_back(sommets[nbSommetsCote * i + j + 1]);
            uneNouvelleFace.push_back(sommets[nbSommetsCote * (i + 1) + j + 1]);
            uneNouvelleFace.push_back(sommets[nbSommetsCote * (i + 1) + j]);

            mesh.add_face(uneNouvelleFace);
        }
    }

    mesh.update_normals();

    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // on calcule la heightmap
    heightmap(&mesh, nbFacesCote);

    color(&mesh);

    // on affiche le maillage
    displayMesh(&mesh);
    this->mesh = mesh;
    ui->vertexSelect_spinBox->setMaximum(mesh.n_vertices());
}

void MainWindow::on_vertexSelect_spinBox_valueChanged(int arg1)
{
    if(arg1 != -1){
        if(vertexSelection == -1)
            ui->vertexSelect_spinBox->setMinimum(0);
        else{
            VertexHandle vh = mesh.vertex_handle(vertexSelection);
            mesh.set_color(vh, MyMesh::Color(0, 0, 0));
            mesh.data(vh).thickness = 1;
        }
    }

    vertexSelection = arg1;
    VertexHandle vh = mesh.vertex_handle(vertexSelection);
    mesh.set_color(vh, MyMesh::Color(225, 0, 0));
    mesh.data(vh).thickness = 7;

    displayMesh(&mesh);
}

void MainWindow::on_toMountain_pushButton_clicked()
{
    if (vertexSelection > 0 && vertexSelection < mesh.n_vertices())
    {
        Mountain m;
        transformVertex(&mesh, mesh.vertex_handle(vertexSelection), &m);
        displayMesh(&mesh);
    }
}

void MainWindow::on_toPlains_pushButton_clicked()
{
    if (vertexSelection > 0 && vertexSelection < mesh.n_vertices())
    {
        Plains m;
        transformVertex(&mesh, mesh.vertex_handle(vertexSelection), &m);
        displayMesh(&mesh);
    }
}

void MainWindow::on_toWater_pushButton_clicked()
{
    if (vertexSelection > 0 && vertexSelection < mesh.n_vertices())
    {
        Water m;
        transformVertex(&mesh, mesh.vertex_handle(vertexSelection), &m);
        displayMesh(&mesh);
    }
}

/* **** fin de la partie boutons et IHM **** */


void MainWindow::transformVertex(MyMesh* _mesh, VertexHandle vh, Biome* b)
{
    float v = _mesh->property(vprop_coeff, vh);
    MyMesh::Point n = _mesh->normal(vh);
    _mesh->set_point(vh, _mesh->point(vh) - v * n);

    v = b->transform(v) * (!isSphere*2-1) + isSphere;
    _mesh->set_point(vh, _mesh->point(vh) + v * n);
    _mesh->property(vprop_coeff, vh) = v;

    for (MyMesh::VertexFaceIter curFace = _mesh->vf_iter(vh); curFace.is_valid(); curFace++)
    {
        colorFace(_mesh, *curFace);
    }

}


/* **** fonctions supplémentaires **** */
// permet d'initialiser les couleurs et les épaisseurs des élements du maillage
void MainWindow::resetAllColorsAndThickness(MyMesh* _mesh)
{
    for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
    {
        _mesh->data(*curVert).thickness = 1;
        _mesh->set_color(*curVert, MyMesh::Color(0, 0, 0));
    }

    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        _mesh->set_color(*curFace, MyMesh::Color(150, 150, 150));
    }

    for (MyMesh::EdgeIter curEdge = _mesh->edges_begin(); curEdge != _mesh->edges_end(); curEdge++)
    {
        _mesh->data(*curEdge).thickness = 1;
        _mesh->set_color(*curEdge, MyMesh::Color(0, 0, 0));
    }
}

// charge un objet MyMesh dans l'environnement OpenGL
void MainWindow::displayMesh(MyMesh* _mesh, bool isTemperatureMap, float mapRange)
{
    GLuint* triIndiceArray = new GLuint[_mesh->n_faces() * 3];
    GLfloat* triCols = new GLfloat[_mesh->n_faces() * 3 * 3];
    GLfloat* triVerts = new GLfloat[_mesh->n_faces() * 3 * 3];

    int i = 0;

    if(isTemperatureMap)
    {
        QVector<float> values;

        if(mapRange == -1)
        {
            for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
                values.append(fabs(_mesh->data(*curVert).value));
            qSort(values);
            mapRange = values.at(values.size()*0.8);
            qDebug() << "mapRange" << mapRange;
        }

        float range = mapRange;
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;

        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }
    else
    {
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;
        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }


    ui->displayWidget->loadMesh(triVerts, triCols, _mesh->n_faces() * 3 * 3, triIndiceArray, _mesh->n_faces() * 3);

    delete[] triIndiceArray;
    delete[] triCols;
    delete[] triVerts;

    GLuint* linesIndiceArray = new GLuint[_mesh->n_edges() * 2];
    GLfloat* linesCols = new GLfloat[_mesh->n_edges() * 2 * 3];
    GLfloat* linesVerts = new GLfloat[_mesh->n_edges() * 2 * 3];

    i = 0;
    QHash<float, QList<int> > edgesIDbyThickness;
    for (MyMesh::EdgeIter eit = _mesh->edges_begin(); eit != _mesh->edges_end(); ++eit)
    {
        float t = _mesh->data(*eit).thickness;
        if(t > 0)
        {
            if(!edgesIDbyThickness.contains(t))
                edgesIDbyThickness[t] = QList<int>();
            edgesIDbyThickness[t].append((*eit).idx());
        }
    }
    QHashIterator<float, QList<int> > it(edgesIDbyThickness);
    QList<QPair<float, int> > edgeSizes;
    while (it.hasNext())
    {
        it.next();

        for(int e = 0; e < it.value().size(); e++)
        {
            int eidx = it.value().at(e);

            MyMesh::VertexHandle vh1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh1)[0];
            linesVerts[3*i+1] = _mesh->point(vh1)[1];
            linesVerts[3*i+2] = _mesh->point(vh1)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;

            MyMesh::VertexHandle vh2 = _mesh->from_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh2)[0];
            linesVerts[3*i+1] = _mesh->point(vh2)[1];
            linesVerts[3*i+2] = _mesh->point(vh2)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;
        }
        edgeSizes.append(qMakePair(it.key(), it.value().size()));
    }

    ui->displayWidget->loadLines(linesVerts, linesCols, i * 3, linesIndiceArray, i, edgeSizes);

    delete[] linesIndiceArray;
    delete[] linesCols;
    delete[] linesVerts;

    GLuint* pointsIndiceArray = new GLuint[_mesh->n_vertices()];
    GLfloat* pointsCols = new GLfloat[_mesh->n_vertices() * 3];
    GLfloat* pointsVerts = new GLfloat[_mesh->n_vertices() * 3];

    i = 0;
    QHash<float, QList<int> > vertsIDbyThickness;
    for (MyMesh::VertexIter vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit)
    {
        float t = _mesh->data(*vit).thickness;
        if(t > 0)
        {
            if(!vertsIDbyThickness.contains(t))
                vertsIDbyThickness[t] = QList<int>();
            vertsIDbyThickness[t].append((*vit).idx());
        }
    }
    QHashIterator<float, QList<int> > vitt(vertsIDbyThickness);
    QList<QPair<float, int> > vertsSizes;

    while (vitt.hasNext())
    {
        vitt.next();

        for(int v = 0; v < vitt.value().size(); v++)
        {
            int vidx = vitt.value().at(v);

            pointsVerts[3*i+0] = _mesh->point(_mesh->vertex_handle(vidx))[0];
            pointsVerts[3*i+1] = _mesh->point(_mesh->vertex_handle(vidx))[1];
            pointsVerts[3*i+2] = _mesh->point(_mesh->vertex_handle(vidx))[2];
            pointsCols[3*i+0] = _mesh->color(_mesh->vertex_handle(vidx))[0];
            pointsCols[3*i+1] = _mesh->color(_mesh->vertex_handle(vidx))[1];
            pointsCols[3*i+2] = _mesh->color(_mesh->vertex_handle(vidx))[2];
            pointsIndiceArray[i] = i;
            i++;
        }
        vertsSizes.append(qMakePair(vitt.key(), vitt.value().size()));
    }

    ui->displayWidget->loadPoints(pointsVerts, pointsCols, i * 3, pointsIndiceArray, i, vertsSizes);

    delete[] pointsIndiceArray;
    delete[] pointsCols;
    delete[] pointsVerts;
}


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


